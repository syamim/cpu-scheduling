const round_robin_obj = {
  processes: [],
  ready_que: [],
  current_index: 0,
  quantum: 3,
  current_time: 0,
  finish_que: []
}
export function roundRobin(testData){
  testData.sort((a, b) => a.arrival_time - b.arrival_time) // sort by arrival time
  round_robin_obj.processes = testData
  round_robin_obj.ready_que.push(testData.shift())
  runNext()
  return 'fuck'
}

function add_process(processObj) {
  round_robin_obj.ready_que.push(processObj);
}

function remove_process() {
  // const task = round_robin_obj.ready_que[round_robin_obj.current_index];
  // round_robin_obj.current_index = (round_robin_obj.current_index + 1) % round_robin_obj.ready_que.length;
  // return task;

  return round_robin_obj.ready_que.shift()
}

function runNext() {
  // if (round_robin_obj.ready_que == 0){
  //   round_robin_obj.current_time++
  // }else{
  //   const process = remove_process()
  //   if (process){
  //     const remainingBurstTime = Math.min(round_robin_obj.quantum, process.burst_time);
  //     finishTime = round_robin_obj.current_time + remainingBurstTime;
  //   }
  // }

  while (round_robin_obj.processes.some(process => process.burst_time > 0)) {
    const task = remove_process();
    if (task) {
      console.table(round_robin_obj.ready_que)
      console.log(`Running task: ${task.process} (Arrival Time: ${task.arrival_time}, Burst Time: ${task.burst_time})`);
  
      // Calculate finish time
      const remainingBurstTime = Math.min(round_robin_obj.quantum, task.burst_time);
      // if burst time already 0, do not add cause it will update the finish time
      let finishTime = 0
      // if (remainingBurstTime !== 0){
        finishTime = round_robin_obj.current_time + remainingBurstTime;
      // }
  
      console.log(`Finish Time: ${finishTime}`);
      round_robin_obj.current_time = finishTime; // Update current time
  
      // Reduce burst time based on the quantum
      task.burst_time -= remainingBurstTime;
      if (task.burst_time > 0) {
        console.log(`Quantum expired. Remaining burst time: ${task.burst_time}`);
        // check for other task arrival
        checkOtherArrival(finishTime)
        // round_robin_obj.ready_que.push(task); // Add the task back to the end of the queue
        add_process(task); // Add the task back to the end of the queue
      } else {
        if (remainingBurstTime !== 0){
          round_robin_obj.finish_que.push({...task, finish_time: finishTime})
        }
        console.log('Task completed.');
      }
      // console.log(`Current Index : ${round_robin_obj.current_index}, Queue Length : ${round_robin_obj.processes.length}`)
      // console.table(round_robin_obj.ready_que)
    } else {
      round_robin_obj.current_time++
      checkOtherArrival()
      console.log('No tasks in the queue.');
    }
  }
  console.log('finish')
  console.table(round_robin_obj.finish_que)
}

function checkOtherArrival(currentProcess){
  for (const process of round_robin_obj.processes) {
    // console.log("🚀 ~ file: App.vue:211 ~ checkProcess ~ process:", process)
    console.log("🚀 ~ file: App.vue:171 ~ checkOtherArrival ~ process:", process)
    if(process.arrival_time <= round_robin_obj.current_time && process.arrival_time < currentProcess.arrival_time){
      add_process(process)
    }
  }
}